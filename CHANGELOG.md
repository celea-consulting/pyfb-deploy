# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Architecture globale du projet


## 0.0.1 - 2018-09-11
### Added
- Ce fichier CHANGELOG gardant l'historique des évolutions du projet
- Un fichier README présentant le projet


Topic's explanations :

Types of changes [Sources](https://keepachangelog.com)

    **Added** for new features.
    **Changed** for changes in existing functionality.
    **Deprecated** for soon-to-be removed features.
    **Removed** for now removed features.
    **Fixed** for any bug fixes.
    **Security** in case of vulnerabilities.
